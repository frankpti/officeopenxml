/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace DocumentFormat.OpenXml
{
	public static class XmlNamespaces
	{
		public const string MlSpreadsheet = "urn:schemas-microsoft-com:office:spreadsheet";
		public const string MlOffice = "urn:schemas-microsoft-com:office:office";
		public const string MlExcel = "urn:schemas-microsoft-com:office:excel";
		public const string OfficeOpenSpreadsheet = "http://schemas.openxmlformats.org/spreadsheetml/2006/main";
        public const string OfficeOpenWordprocessingDocument = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        public const string OfficeOpenDrawing = "http://schemas.openxmlformats.org/drawingml/2006/main";
        public const string OfficeOpenDrawingSpreadsheet = "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing";
        public const string OfficeOpenDrawingChart = "http://schemas.openxmlformats.org/drawingml/2006/chart";
		public const string OfficeOpenPackageRelationships = "http://schemas.openxmlformats.org/package/2006/relationships";
        public const string OfficeOpenPackageMetadataCoreProperties = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
        public const string OfficeOpenDocumentExtendedProperties = "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties";
		public const string OfficeOpenDocumentRelationships = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
		public const string OfficeOpenRelationshipDocument = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument";
		public const string OfficeOpenRelationshipWorksheet = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet";
        public const string OfficeOpenRelationshipDrawing = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing";
        public const string OfficeOpenRelationshipChart = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart";
		public const string OfficeOpenRelationshipSharedStrings = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings";
        public const string OfficeOpenRelationshipStyles = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles";
        public const string OfficeOpenRelationshipCalcChain = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/calcChain";
        public const string OfficeOpenRelationshipSettings = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings";
        public const string OfficeOpenRelationshipFontTable = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable";
        public const string OfficeOpenRelationshipMetadataCoreProperties = "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties";
        public const string OfficeOpenRelationshipExtendedProperties = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties";
        public const string OfficeOpenRelationshipCustomXmlData = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXmlData";
        public const string DcElements = "http://purl.org/dc/elements/1.1/";
        public const string DcTerms = "http://purl.org/dc/terms/";
        public const string DcmiType = "http://purl.org/dc/dcmitype/";
        public const string Xsi = "http://www.w3.org/2001/XMLSchema-instance";
        public const string Xml = "http://www.w3.org/XML/1998/namespace";
	}
}

