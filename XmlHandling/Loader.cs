using Logging;
/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;
using YAXLib;

namespace DocumentFormat.OpenXml.XmlHandling
{
    public static class Loader
    {
        const string loadFromXmlMethodName = "LoadFromXml";
        const string addMethodName = "Add";

        internal static T Load<T>(XmlNode node)
        {
            T result = default(T);
            if (node.Attributes.Count > 0) {
                result = LoadAttributes<T>(node, result);
            }
            if (node.HasChildNodes) {
                result = LoadChildren<T>(node, result);
            }
            return result;
        }

        internal static T LoadAttributes<T>(XmlNode node, T t)
        {
            T result = t;
            if (result == null) {
                result = Activator.CreateInstance<T>();
            }

            Dictionary<string, PropertyInfo> properties = LoadProperties(typeof(T));

            foreach (XmlAttribute attribute in node.Attributes) {
                if (properties.ContainsKey(attribute.LocalName)) {
                    PropertyInfo property = properties[attribute.LocalName];
                    object[] serializeAsAttribute = property.GetCustomAttributes(typeof(YAXSerializeAsAttribute), false);
                    Type propertyType = property.PropertyType;
                    if (propertyType.IsGenericType) {
                        LoadGeneric<T>(ref result, properties, propertyType, attribute);
                    } else {
                        LoadObject<T>(ref result, properties, propertyType, attribute);
                    }
                } else {
                    Logger.Warning("OOXML Loader: Property for attribute '{0}' not found", attribute.LocalName);
                }
            }

            return result;
        }

        internal static T LoadChildren<T>(XmlNode node, T t)
        {
            T result = t;
            if (result == null) {
                result = Activator.CreateInstance<T>();
            }

            Dictionary<string, PropertyInfo> properties = LoadProperties(typeof(T));

            object listObject = null;   // object for children of root with "recursive without containing element"

            foreach (XmlNode child in node.ChildNodes) {
                if (properties.ContainsKey(child.LocalName)) {
                    PropertyInfo property = properties[child.LocalName];

                    object[] collectionAttribute = property.GetCustomAttributes(typeof(YAXCollectionAttribute), false);
                    object[] serializeAsAttribute = property.GetCustomAttributes(typeof(YAXSerializeAsAttribute), false);
                    Type propertyType = property.PropertyType;
                    if (collectionAttribute.Length > 0 && serializeAsAttribute.Length == 0) {
                        if (listObject == null) {
                            listObject = Activator.CreateInstance(propertyType);
                        }
                        AddToList(ref listObject, propertyType, child);
                    }

                    if (propertyType.IsGenericType) {
                        LoadGeneric<T>(ref result, properties, propertyType, child);
                    } else {
                        LoadObject<T>(ref result, properties, propertyType, child);
                    }
                } else {
                    Logger.Warning("OOXML Loader: Property for child '{0}' not found", child.LocalName);
                }
            }

            if (listObject != null) {
                MethodInfo setter = properties[node.FirstChild.LocalName].GetSetMethod();
                setter.Invoke(result, new object[] { listObject });
            }

            return result;
        }

        internal static void AddToList(ref object listObject, Type propertyType, XmlNode child)
        {
            Type genericArg = propertyType.GetGenericArguments()[0];
            MethodInfo loadMethod = genericArg.GetMethod(loadFromXmlMethodName);

            if (loadMethod != null) {
                object val = loadMethod.Invoke(null, new object[] { child });

                MethodInfo addMethod = propertyType.GetMethod(addMethodName);
                addMethod.Invoke(listObject, new object[] { val });
            }
        }

        internal static void LoadGeneric<T>(
            ref T result, Dictionary<string, PropertyInfo> properties, Type propertyType, XmlNode child)
        {
            Type[] genericArgs = propertyType.GetGenericArguments();

            switch (genericArgs.Length) {
                case 1:
                    if (propertyType.Equals(typeof(List<>).MakeGenericType(genericArgs))) {
                        LoadGenericList<T>(ref result, properties, propertyType, child);
                    } else if (propertyType.Equals(typeof(Nullable<>).MakeGenericType(genericArgs))) {
                        LoadObject<T>(ref result, properties, genericArgs[0], child);
                    } else {
                        Logger.Warning("OOXML Loader: No suitable class with {0} generic argument(s) for type: '{1}'", genericArgs.Length, propertyType);
                    }
                    break;
                case 2:
                    Logger.Warning("OOXML Loader: No suitable class with {0} generic argument(s) for type: '{1}'", genericArgs.Length, propertyType);
                    break;
                default:
                    Logger.Warning("OOXML Loader: No suitable classes for {0} generic arguments for type: '{1}'", genericArgs.Length, propertyType);
                    break;
            }
        }

        internal static void LoadGenericList<T>(
            ref T result, Dictionary<string, PropertyInfo> properties, Type propertyType, XmlNode child)
        {
            Type genericArg = propertyType.GetGenericArguments()[0];
            MethodInfo loadMethod = genericArg.GetMethod(loadFromXmlMethodName);

            if (loadMethod != null) {
                object instance = Activator.CreateInstance(propertyType);
                MethodInfo addMethod = propertyType.GetMethod(addMethodName);
                foreach (XmlNode grandChild in child.ChildNodes) {
                    try {
                        object data = loadMethod.Invoke(null, new object[] { grandChild });
                        addMethod.Invoke(instance, new object[] { data });
                    } catch (Exception e) {
                        Logger.Log(e);
                        Logger.Log(e.InnerException, "Inner exception");
                    }
                }
                MethodInfo setter = properties[child.LocalName].GetSetMethod();
                setter.Invoke(result, new object[] { instance });
            } else {
                StringBuilder sb = new StringBuilder();
                sb.Append(String.Format(
                    "OOXML Loader: No method named '{0}' found in type '{0}': ", loadFromXmlMethodName, genericArg));
                foreach (MethodInfo m in genericArg.GetMethods()) {
                    sb.Append(m.Name);
                    sb.Append(" ");
                }
                Logger.Error(sb.ToString());
            }
        }

        internal static void LoadObject<T>(
            ref T result, Dictionary<string, PropertyInfo> properties, Type propertyType, XmlNode child)
        {
            MethodInfo loadmethod = propertyType.GetMethod(loadFromXmlMethodName);

            object[] parameters = null;
            string childValue = child.Value;
            if (childValue == null && child.Attributes.Count > 0 && child.Attributes[0].LocalName == "val") {
                childValue = child.Attributes[0].Value;
            }

            if (loadmethod != null) {
                object instance = loadmethod.Invoke(null, new object[] { child });
                parameters = new object[] { instance };
            } else if (propertyType.IsEnum) {
                Dictionary<string, FieldInfo> enumData = LoadEnumeration(propertyType);
                if (enumData.ContainsKey(childValue)) {
                    parameters = new object[] { enumData[childValue].GetValue(propertyType) };
                } else {
                    Logger.Warning("OOXML Loader: Cannot find field '{0}' in enum '{1}'", childValue, propertyType);
                }
            } else if (propertyType.Equals(typeof(String))) {
                parameters = new object[] { childValue };
            } else if (propertyType.Equals(typeof(Double))) {
                Double val = Double.Parse(childValue);
                parameters = new object[] { val };
            } else if (propertyType.Equals(typeof(UInt32))) {
                UInt32 val = UInt32.Parse(childValue);
                parameters = new object[] { val };
            } else if (propertyType.Equals(typeof(Int32))) {
                Int32 val = Int32.Parse(childValue);
                parameters = new object[] { val };
            } else if (propertyType.Equals(typeof(Boolean))) {
                Boolean val;
                if (!Boolean.TryParse(childValue, out val)) {
                    val = (childValue == "1");
                }
                parameters = new object[] { val };
            } else {
                Logger.Warning("OOXML Loader: No method '{0}' found in type '{1}'", loadFromXmlMethodName, propertyType);
            }
            if (parameters != null) {
                MethodInfo setter = properties[child.LocalName].GetSetMethod();
                if (setter != null) {
                    setter.Invoke(result, parameters);
                } else {
                    Logger.Warning("OOXML Loader: Cannot find set method for '{0}'", child.LocalName);
                }
            }
        }

        internal static Dictionary<string, PropertyInfo> LoadProperties(Type clazz)
        {
            Dictionary<string, PropertyInfo> result = new Dictionary<string, PropertyInfo>();

            object[] serializeTypeAttributes = clazz.GetCustomAttributes(typeof(YAXSerializableTypeAttribute), false);
            YAXSerializationFields fieldsToSerialize = YAXSerializationFields.AllFields;
            if (serializeTypeAttributes.Length > 0) {
                YAXSerializableTypeAttribute serializeType = (YAXSerializableTypeAttribute)serializeTypeAttributes[0];
                fieldsToSerialize = serializeType.FieldsToSerialize;
            }
            PropertyInfo[] properties = null;
            switch (fieldsToSerialize) {
                case YAXSerializationFields.AllFields:
                case YAXSerializationFields.AttributedFieldsOnly:
                    properties = clazz.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    break;
                case YAXSerializationFields.PublicPropertiesOnly:
                    properties = clazz.GetProperties(BindingFlags.Public);
                    break;
            }

            foreach (PropertyInfo property in properties) {
                object[] serializableAttributes = property.GetCustomAttributes(typeof(YAXSerializableFieldAttribute), false);
                bool serializable = serializableAttributes.Length > 0;
                string serializeAs = property.Name;

                object[] serializeAsAttribtes = property.GetCustomAttributes(typeof(YAXSerializeAsAttribute), false);
                object[] collectionAttributes = property.GetCustomAttributes(typeof(YAXCollectionAttribute), false);

                if (serializeAsAttribtes.Length > 0) {
                    object[] attrForAttributes = property.GetCustomAttributes(typeof(YAXAttributeForAttribute), false);
                    if (attrForAttributes.Length > 0) {
                        serializeAs = ((YAXAttributeForAttribute)attrForAttributes[0]).Parent;
                    } else {
                        serializeAs = ((YAXSerializeAsAttribute)serializeAsAttribtes[0]).SerializeAs;
                    }
                } else if (collectionAttributes.Length > 0) {
                    YAXCollectionAttribute attr = (YAXCollectionAttribute)collectionAttributes[0];
                    serializeAs = attr.EachElementName;
                }

                if (fieldsToSerialize != YAXSerializationFields.AttributedFieldsOnly ||
                    (fieldsToSerialize == YAXSerializationFields.AttributedFieldsOnly && serializable)) {
                    if (property.CanWrite) {
                        if (result.ContainsKey(serializeAs)) {
                            Logger.Log(
                                "OOXML loader: Dictionary already contains key '{0}' for property '{1}'",
                                serializeAs, property);
                        } else {
                            result.Add(serializeAs, property);
                        }
                    } else {
                        Logger.Log(
                            "OOXML Loader: Ignoring '{0}' in '{1}' because it is not writable",
                            property.Name, clazz.FullName);
                    }
                }
            }

            return result;
        }

        internal static Dictionary<string, FieldInfo> LoadEnumeration(Type enumType)
        {
            Dictionary<string, FieldInfo> result = new Dictionary<string, FieldInfo>();
            foreach (FieldInfo field in enumType.GetFields()) {
                YAXEnumAttribute[] attributes = (YAXEnumAttribute[])field.GetCustomAttributes(typeof(YAXEnumAttribute), false);
                if (attributes.Length > 0) {
                    result.Add(attributes[0].Alias, field);
                } else {
                    result.Add(field.Name, field);
                }
            }
            return result;
        }
    }
}

