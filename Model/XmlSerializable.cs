/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO;
using System.Text;
using YAXLib;
using System.Xml;
using Frank.Helpers.Xml;

namespace DocumentFormat.OpenXml.Model
{
    public abstract class XmlSerializable
    {
        public void SerializeStreamXml(Stream writeStream)
        {
            try {
                YAXSerializer serializer = new YAXSerializer(GetType());
                XmlWriterSettings settings = new XmlWriterSettings() {
                    Indent = true
                };
                MemoryStream testStream = new MemoryStream();
                NamespaceStrippingXmlWriter xmlWriter = 
                    new NamespaceStrippingXmlWriter(
                        new LowercaseBoolXmlWriter(testStream, settings), "yaxlib", "http://www.sinairv.com/yaxlib/");
                serializer.Serialize(this, xmlWriter);
                xmlWriter.Flush();
                xmlWriter.Close();
                byte[] buffer = testStream.GetBuffer();
                writeStream.Write (buffer, 0, (int)testStream.Length);
            } catch (Exception e) {
                Console.WriteLine(e); 
                throw;
            }
        }

        public string SerializeString()
        {
            MemoryStream stream = new MemoryStream();
            SerializeStreamXml(stream);
            return Encoding.UTF8.GetString(stream.GetBuffer());
        }

        public static T DeserializeStreamXml<T>(Stream readStream)
        {
            YAXSerializer serializer = new YAXSerializer(typeof(T));
            using (XmlTextReader xmlReader = new XmlTextReader(readStream)) {
                return (T)serializer.Deserialize(xmlReader);
            }
        }
    }
}

