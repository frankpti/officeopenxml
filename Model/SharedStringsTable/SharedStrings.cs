/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using YAXLib;
using DocumentFormat.OpenXml.Model.Worksheets;
using System.IO;

namespace DocumentFormat.OpenXml.Model.SharedStringsTable
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("sst")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class SharedStrings: PackageXmlSerializable, IPart
    {
        internal const string DefaultDocumentPath = "/xl/sharedStrings.xml";

        public SharedStrings ()
        {
            Strings = new List<SharedString>();
            Count = 0;
        }

        public SharedStrings(string relId):
            this()
        {
            RelId = relId;
        }

        List<SharedString> strings;
        [YAXSerializableField]
        [YAXCollection (YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "si")]
        public List<SharedString> Strings {
            get {
                return strings;
            }
            set {
                strings = value;
                Count = strings.Count;
            }
        }

        public int GetIndexOf(SharedString s, bool increaseCount) {
            if (Strings.Contains (s)) {
                if (increaseCount) {
                    Count++;
                }
                return Strings.IndexOf(s);
            }
            Count++;
            Strings.Add (s);
            return Strings.Count - 1;
        }

        public StringCell CreateCell(string text, uint rowNum, uint colNum)
        {
            return this.CreateCell(text, 0, rowNum, colNum);
        }

        public StringCell CreateCell (string text, uint style, uint rowNum, uint colNum)
        {
            if (text == null) {
                return null;
            }
            StringCell cell = new StringCell(this, text, rowNum, colNum);
            cell.Style = style;
            return cell;
        }

        public override string ContentType
        {
            get {
                return ContentTypes.SharedStringsXml;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeForClass]
        public int Count {
            get;
            private set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("uniqueCount")]
        [YAXAttributeForClass]
        public int Unique {
            get {
                return Strings.Count;
            }
        }

        public void Serialize (Stream stream)
        {
            SerializeStreamXml(stream);
        }

        public string DocumentPath {
            get {
                return DefaultDocumentPath;
            }
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipSharedStrings;
            }
        }
    }
}

