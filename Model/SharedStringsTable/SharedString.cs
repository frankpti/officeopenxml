/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Xml;

namespace DocumentFormat.OpenXml.Model.SharedStringsTable
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("si")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class SharedString
    {
        public SharedString():
            this(String.Empty)
        {
        }

        public SharedString (string text)
        {
            this.Text = text;
        }

        [YAXSerializableField]
        [YAXSerializeAs("t")]
        public string Text {
            get;
            private set;
        }

        public static implicit operator SharedString(string s) {
            return new SharedString(s);
        }

        public static bool operator == (SharedString a, SharedString b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.Text == b.Text;
        }

        public static bool operator != (SharedString a, SharedString b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return Text;
        }

        public override bool Equals (object obj)
        {
            if (obj is SharedString) {
                return ((obj as SharedString) == this);
            }
            return Text.Equals (obj);
        }

        public override int GetHashCode ()
        {
            return Text.GetHashCode ();
        }

        public static SharedString LoadFromXml(XmlNode node)
        {
            return new SharedString(node.InnerText);
        }
    }
}

