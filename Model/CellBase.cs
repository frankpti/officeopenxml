/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("c")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public abstract class CellBase
    {
        private const string AlphabetChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public CellBase (uint rowNum, uint colNum)
        {
            RowNum = rowNum;
            ColNum = colNum;
        }

        public uint RowNum {
            get;
            private set;
        }

        public uint ColNum {
            get;
            private set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("r")]
        [YAXAttributeForClass]
        public string Reference {
            get {
                return CalculateCellName(RowNum, ColNum);
            }
            set {
                uint rowNum, colNum;
                ParseCellName(value, out rowNum, out colNum);
                RowNum = rowNum;
                ColNum = colNum;
            }
        }

        private static string CalculateColName(int colNum)
        {
            string result = "";

            do {
                int index = (int)(colNum % AlphabetChars.Length);
                result = AlphabetChars[index] + result;
                colNum = colNum / AlphabetChars.Length;
                colNum--;
            } while (colNum >= 0);
            return result;
        }

        public static string CalculateCellName(uint rowNum, uint colNum)
        {
            return CalculateCellName(rowNum, colNum, false, false);
        }

        public static string CalculateCellName(uint rowNum, uint colNum, bool absoluteColumn, bool absoluteRow)
        {
            string formulaFormat = "";
            if (absoluteColumn) {
                formulaFormat += "$";
            }
            formulaFormat += "{0}";
            if (absoluteRow) {
                formulaFormat += "$";
            }
            formulaFormat += "{1}";
            return String.Format(formulaFormat, CalculateColName((int) colNum), rowNum + 1);
        }

        public static void ParseCellName(string cellName, out uint rowNum, out uint colNum)
        {
            cellName = cellName.Replace("$", "");

            rowNum = 0;
            int index = cellName.Length - 1;
            while (index >= 0 && Char.IsDigit(cellName, index)) {
                int digit = int.Parse(cellName.Substring(index, 1));
                int place = (int)Math.Pow(10, cellName.Length - 1 - index);
                rowNum = rowNum + (uint)(digit * place);
                index--;
            }
            rowNum--;
            int colNameLength = index + 1;
            colNum = 0;
            while (index >= 0 && Char.IsLetter(cellName, index)) {
                char ch = Char.ToUpperInvariant(cellName[index]);
                int digit = AlphabetChars.IndexOf(ch);
                int place = (int)Math.Pow(AlphabetChars.Length, colNameLength - 1 - index);
                colNum = colNum + (uint)(digit * place);
                index--;
            }
        }
    }
}

