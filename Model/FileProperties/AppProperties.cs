/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.FileProperties
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("Properties")]
    [YAXNamespace(XmlNamespaces.OfficeOpenDocumentExtendedProperties)]
    public class AppProperties: PackageXmlSerializable, IPart
    {
        public AppProperties ()
        {
        }

        [YAXSerializableField]
        [YAXSerializeAs("TotalTime")]
        public int TotalTime {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("Manager")]
        public string Manager {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("Company")]
        public string Company {
            get;
            set;
        }

        #region implemented abstract members of ProbeNet.Mini.Export.OfficeOpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.ExtendedPropertiesXml;
            }
        }
        #endregion

        #region IPart implementation
        public void Serialize (System.IO.Stream stream)
        {
            SerializeStreamXml(stream);
        }

        public string DocumentPath {
            get {
                return "/docProps/app.xml";
            }
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipExtendedProperties;
            }
        }
        #endregion
    }
}

