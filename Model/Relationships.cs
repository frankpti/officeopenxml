/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;

namespace DocumentFormat.OpenXml.Model
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("Relationships")]
    [YAXNamespace(XmlNamespaces.OfficeOpenPackageRelationships)]
    public class Relationships: PackageXmlSerializable, IStreamSerializable
    {
        public Relationships ()
        {
            ChildrenDictionary = new Dictionary<string, Relationship>();
        }

        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "Relationship")]
        public List<Relationship> Children {
            get {
                return new List<Relationship>(ChildrenDictionary.Values);
            }
            set {
                ChildrenDictionary = new Dictionary<string, Relationship>();
                foreach (Relationship relationship in value) {
                    ChildrenDictionary.Add(relationship.Id, relationship);
                }
            }
        }

        private Dictionary<string, Relationship> ChildrenDictionary {
            get;
            set;
        }

        public override string ContentType
        {
            get {
                return ContentTypes.RelationshipsXml;
            }
        }

        public string NextId ()
        {
            int i = 1;
            IList<string> usedIds = GetIds();
            while (usedIds.Contains(String.Format("rId{0}", i))) {
                i++;
            }
            return String.Format("rId{0}", i);
        }

        public Relationship Append(string type, string target)
        {
            return Relationship.Create(this, type, target);
        }

        public Relationship Insert(IPart part)
        {
            return Relationship.Create(this, part);
        }

        public Relationship this[string relId] {
            get {
                return ChildrenDictionary[relId];
            }
            set {
                if (relId != value.Id) {
                    throw new ArgumentException(
                        String.Format("Relationship Id of value ('{0}') does not match index ('{1}')", value.Id, relId), "relId");
                }
                if (ChildrenDictionary.ContainsKey(relId)) {
                    ChildrenDictionary[relId] = value;
                } else {
                    ChildrenDictionary.Add(relId, value);
                }
            }
        }

        public IList<string> GetIds()
        {
            return new List<string>(ChildrenDictionary.Keys);
        }

        public Relationship FindByTarget(string target)
        {
            foreach (Relationship relationship in ChildrenDictionary.Values) {
                if (relationship.Target.TrimStart('/') == target.TrimStart('/')) {
                    return relationship;
                }
            }
            return null;
        }

        public IList<Relationship> FindByType(string type)
        {
            IList<Relationship> list = new List<Relationship>();
            foreach (Relationship relationship in ChildrenDictionary.Values) {
                if (relationship.Type == type) {
                    list.Add(relationship);
                }
            }
            return list;
        }

        public void RemoveAllByType(string type)
        {
            IList<Relationship> list = FindByType(type);
            foreach (Relationship r in list) {
                ChildrenDictionary.Remove(r.Id);
            }
        }

        public void RemoveByTarget (string target)
        {
            Relationship rel = FindByTarget (target);
            if (rel != null) {
                ChildrenDictionary.Remove(rel.Id);
            }
        }

        #region IStreamSerializable implementation
        public void Serialize (System.IO.Stream stream)
        {
            SerializeStreamXml(stream);
        }
        #endregion
    }
}

