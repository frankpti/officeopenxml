/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    public static class HeaderFooterCode
    {
        public const string Amp = "&";

        public const string LeftSection = Amp + "L";
        public const string CenterSection = Amp + "C";
        public const string RightSection = Amp + "R";

        public const string CurrentPage = Amp + "P";
        public const string TotalPages = Amp + "N";
        public const string TextStrikethroughOn = Amp + "S";
        public const string TextStrikethroughOff = TextStrikethroughOn;
        public const string TextSuperscriptOn = Amp + "X";
        public const string TextSuperscriptOff = TextSuperscriptOn;
        public const string TextSubscriptOn = Amp + "Y";
        public const string TextSubscriptOff = TextSubscriptOn;
        public const string Date = Amp + "D";
        public const string Time = Amp + "T";
        public const string PictureAsBackground = Amp + "G";
        public const string TextSingleUnderlineOn = Amp + "U";
        public const string DoubleUnderlineOn = Amp + "E";
        public const string WorkbookFilePath = Amp + "Z";
        public const string WorkbookFileName = Amp + "F";
        public const string SheetTabName = Amp + "A";
        public const string OutlineStyle = Amp + "O";
        public const string ShadowStyle = Amp + "H";

        public static string TextFontSize(int size)
        {
            return String.Format("{0}{1}", Amp, size);
        }

        public static string TextFontColorRgb(uint red, uint green, uint blue)
        {
            red &= 0xFF;
            green &= 0xFF;
            blue &= 0xFF;
            return TextFontColorRgb((red << 8) | (green << 4) | blue);
        }

        public static string TextFontColorRgb(uint rgb)
        {
            rgb &= 0xFFFFFF;
            return String.Format("{0}K{1:X06}", Amp, rgb);
        }

        public static string AddToPageNumber(int val)
        {
            return String.Format("{0}+{1}", Amp, val);
        }

        public static string SubtractFromPageNumber(int val)
        {
            return String.Format("{0}-{1}", Amp, val);
        }

        public static string TextFontNameAndType(string fontName, string fontType)
        {
            return String.Format("{0}\"{1},{2}\"", Amp, fontName, fontType);
        }

        public static string BoldFontStyle {
            get {
                return TextFontNameAndType("-", "Bold");
            }
        }

        public static string ItalicFontStyle {
            get {
                return TextFontNameAndType("-", "Italic");
            }
        }

        public static string BoldItalicFontStyle {
            get {
                return TextFontNameAndType("-", "Bold Italic");
            }
        }

        public static string RegularFontStyle {
            get {
                return TextFontNameAndType("-", "Regular");
            }
        }

        public static string TextSingleUnderlineOff {
            get {
                return RegularFontStyle;
            }
        }

        public static string DoubleUnderlineOff {
            get {
                return RegularFontStyle;
            }
        }
    }
}

