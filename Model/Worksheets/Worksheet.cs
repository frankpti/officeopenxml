/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Model.SharedStringsTable;
using DocumentFormat.OpenXml.Model.Styles;
using System.IO;

namespace DocumentFormat.OpenXml.Model.Worksheets
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("worksheet")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Worksheet: PackageXmlSerializable, IPart
    {
        public Worksheet(SharedStrings sharedStrings, StyleSheet styles, string documentPath, string relId)
        {
            SheetData = new SheetData();
            SharedStrings = sharedStrings;
            Styles = styles;
            DocumentPath = documentPath;
            RelId = relId;
        }

        public Worksheet(SharedStrings sharedStrings, StyleSheet styles, string documentPath):
            this(sharedStrings, styles, documentPath, null)
        {
        }

        [YAXSerializableField]
        [YAXSerializeAs("cols")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "col")]
        public List<Column> Columns {
            get;
            private set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("sheetData")]
        public SheetData SheetData {
            get;
            private set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("pageSetup")]
        public PageSetup PageSetup {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("headerFooter")]
        public HeaderFooter HeaderFooter {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("drawing")]
        public Drawing Drawing {
            get;
            set;
        }

        protected SharedStrings SharedStrings {
            get;
            private set;
        }

        protected StyleSheet Styles {
            get;
            private set;
        }

        public override string ContentType
        {
            get {
                return ContentTypes.WorksheetXml;
            }
        }

        protected void AddColumnInformation(int[] stringLengths, uint offset = 0)
        {
            for (uint i = 0; i < stringLengths.Length; i++) {
                if (stringLengths[i] != 0) {
                    AddColumnInformation(i + offset + 1, stringLengths[i]);
                }
            }
        }

        protected void AddColumnInformation(uint x, int stringLength)
        {
            AddColumnInformation(x, x, stringLength);
        }
            
        protected void AddColumnInformation (uint minimum, uint maximum, int stringLength)
        {
            if (Columns == null) {
                Columns = new List<Column>();
            }

            double width = Column.CalculateWidth(stringLength);
            Column column = null;
            foreach (Column searchColumn in Columns) {
                if (searchColumn.Minimum == minimum && searchColumn.Maximum == maximum) {
                    column = searchColumn;
                }
            }

            if (column == null) {
                column = new Column () {
                    Minimum = minimum,
                    Maximum = maximum,
                    Width = width
                };
                Columns.Add(column);
            } else {
                column.Width = Math.Max (column.Width, width);
            }
        }

        public void Serialize (Stream stream)
        {
            SerializeStreamXml(stream);
        }

        public string DocumentPath {
            get;
            private set;
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipWorksheet;
            }
        }

        public static string BuildDocumentPathFromFilename(string filename)
        {
            if (!filename.EndsWith(".xml")) {
                filename += ".xml";
            }
            return String.Format("/xl/worksheets/{0}", filename);
        }
    }
}
