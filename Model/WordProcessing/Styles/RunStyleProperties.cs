/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using System.Globalization;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("rPr")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public class RunStyleProperties
	{
        [YAXSerializableField]
        [YAXSerializeAs("rFonts")]
        public RunFonts RunFonts {
            get;
            set;
        }

        public bool Bold {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("b")]
        public object BoldForExport {
            get {
                return Bold ? new object() {} : null;
            } set {
                Bold = (value != null);
            }
        }

        public uint Color {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeFor("color")]
        [YAXSerializeAs("val")]
        public string ColorString {
            get {
                if (Color > 0xFFFFFF) {
                    return "auto";
                }
                return String.Format("{0:X6}", Color);
            }
            set {
                uint hexValue;
                if (value != null && value != "auto" && uint.TryParse (value, NumberStyles.HexNumber, CultureInfo.InvariantCulture.NumberFormat, out hexValue)) {
                    Color = hexValue;
                } else {
                    Color = uint.MaxValue;
                }
            }
        }

        public bool Italics {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("i")]
        public object ItalicsForExport {
            get {
                return Italics ? new object() {} : null;
            } set {
                Italics = (value != null);
            }
        }


        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("sz")]
        public uint FontSizeNonComplexInHalfPoints {
            get;
            set;
        }

        public double FontSizeNonComplexInPoints {
            get {
                return FontSizeNonComplexInHalfPoints / 2.0;
            }
            set {
                FontSizeNonComplexInHalfPoints = (uint) Math.Round(value * 2);
            }
        }
	}
}

