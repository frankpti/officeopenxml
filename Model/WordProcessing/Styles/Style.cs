/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.WordProcessing.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("style")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
	public abstract class Style<TBasedOn, TNext, TLink>: IStyle
        where TBasedOn: IStyle
        where TNext: IStyle
        where TLink: IStyle
	{
        protected Style(StyleType styleType)
        {
            StyleType = styleType;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("type")]
        public StyleType StyleType {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("default")]
        public bool Default {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("styleId")]
        public string StyleId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeFor("name")]
        [YAXSerializeAs("val")]
        public string Name {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForAttribute("basedOn")]
        [YAXSerializeAs("val")]
        public string BasedOn {
            get {
                if (BaseStyle == null) {
                    return null;
                }
                return BaseStyle.StyleId;
            }
        }

        public TBasedOn BaseStyle {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeFor("next")]
        [YAXSerializeAs("val")]
        public string NextStyleId {
            get {
                if (Next == null) {
                    return null;
                }
                return Next.StyleId;
            }
        }

        public TNext Next {
            get;
            set;
        }

        public string LinkeStyleId {
            get {
                if (Link == null) {
                    return null;
                }
                return Link.StyleId;
            }
        }

        public TLink Link {
            get;
            set;
        }
	}
}

