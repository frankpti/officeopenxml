/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.WordProcessing.RunContent
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("t")]
    [YAXNamespace("w", XmlNamespaces.OfficeOpenWordprocessingDocument)]
    public class Text: ITextRunContent
    {
        public Text (string text)
        {
            Contents = text;
            PreserveWhitespace = PreserveWhitespaceType.Default;
        }

        [YAXSerializableField]
        [YAXValueForClass]
        public string Contents {
            get;
            set;
        }
        /*
        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("space")]
        [YAXNamespace("xml", XmlNamespaces.Xml)]*/
        public PreserveWhitespaceType PreserveWhitespace {
            get;
            set;
        }

        public static implicit operator string(Text text)
        {
            return text.Contents;
        }

        public static implicit operator Text(string text)
        {
            return new Text(text);
        }
    }
}

