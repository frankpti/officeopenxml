using DocumentFormat.OpenXml.Model.Worksheets;
using DocumentFormat.OpenXml.XmlHandling;
using System.Xml;
/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("stop")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class GradientStop
    {
        public GradientStop() :
            this(new Color("FF000000"), 0)
        {
        }

        public GradientStop(Color color, double position)
        {
            Color = color;
            Position = position;
        }

        [YAXSerializableField]
        [YAXSerializeAs("color")]
        public Color Color
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("position")]
        [YAXAttributeForClass]
        public double Position
        {
            get;
            set;
        }

        public static bool operator ==(GradientStop a, GradientStop b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return ((a.Color == b.Color) && (a.Position == b.Position));
        }

        public static bool operator !=(GradientStop a, GradientStop b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (obj is GradientStop) {
                return ((GradientStop)obj == this);
            }
            return false;
        }

        public override string ToString()
        {
            return string.Format("[GradientStop: Color={0}, Position={1}]", Color, Position);
        }

        public override int GetHashCode()
        {
            return Color.GetHashCode() ^ Position.GetHashCode();
        }

        public static GradientStop LoadFromXml(XmlNode node)
        {
            return Loader.Load<GradientStop>(node);
        }
    }
}

