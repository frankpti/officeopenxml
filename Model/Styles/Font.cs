/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using DocumentFormat.OpenXml.Model.Worksheets;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("numFmt")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
	public class Font
	{
        private int charset;

        public Font():
            this(String.Empty, null, 0)
        {
        }

        public Font(string name, Nullable<FontFamilyType> family, double size):
            this(name, CharsetType.DefaultCharset, family, size)
        {
        }

        public Font(string name, CharsetType charset, Nullable<FontFamilyType> family, double size)
        {
            Name = name;
            Charset = charset;
            Family = family;
            Size = size;
            FontScheme = FontSchemeType.Minor;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("name")]
        public string Name {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("charset")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public int CharsetValue {
            get { 
                return charset;
            }
            set {
                if (value < 0 || value > 255) {
                    throw new ArgumentOutOfRangeException("value", value, "Charset value is restricted to values ranging from 0 to 255 ");
                }
                charset = value;
            }
        }

        public CharsetType Charset {
            get {
                return (CharsetType) Enum.ToObject(typeof(CharsetType), CharsetValue);
            }
            set {
                CharsetValue = Convert.ToInt32(value);
            }
        }

        public Nullable<FontFamilyType> Family {
            get;
            set;
        }
                
        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("family")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Nullable<int> FamilyValue {
            get {
                if (Family == null) {
                    return null;
                }
                return Convert.ToInt32(Family.Value);
            }
            set {
                if (value == null) {
                    Family = null;
                }
                else
                {
                    Family = (FontFamilyType)value.Value;
                }
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("sz")]
        public double Size {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs ("b")]
        [YAXErrorIfMissed (YAXExceptionTypes.Ignore)]
        public CharacterStyleItem Bold {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("i")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public CharacterStyleItem Italic {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("u")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public UnderlineTypeItem Underline {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("strike")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public CharacterStyleItem StrikeThrough {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("shadow")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public CharacterStyleItem Shadow {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("scheme")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public FontSchemeType FontScheme {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("outline")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public CharacterStyleItem Outline
        {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("extend")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public CharacterStyleItem Extend {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("condense")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public CharacterStyleItem Codense {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("color")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Color Color {
            get;
            set;
        }

        public static bool operator == (Font a, Font b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return (a.Name == b.Name) && (a.Charset == b.Charset) && (a.Family == b.Family) && (a.Size == b.Size) &&
                    (a.Bold == b.Bold) && (a.Italic == b.Italic) && (a.Underline == b.Underline) &&
                    (a.StrikeThrough == b.StrikeThrough) && (a.Shadow == b.Shadow) && (a.FontScheme == b.FontScheme) &&
                    (a.Outline == b.Outline) && (a.Extend == b.Extend) && (a.Codense == b.Codense) &&
                    (a.Extend == b.Extend) && (a.Color == b.Color);
        }

        public static bool operator != (Font a, Font b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Font: Name={0}, Charset={1}, Family={2}, Size={3}, Bold={4}, Italic={5}, " +
                "Underline={6}, StrikeThrough={7}, Shadow={8}, FontScheme={9}, Outline={10}, " +
                "Extend={11}, Codense={12}, Color={13}]", Name, Charset, Family, Size, Bold, Italic, Underline,
                StrikeThrough, Shadow, FontScheme, Outline, Extend, Codense, Color);
        }

        public override bool Equals (object obj)
        {
            if (obj is Font) {
                return ((obj as Font) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return Name.GetHashCode() ^ Charset.GetHashCode() ^ Family.GetHashCode() ^ Size.GetHashCode() ^
                    Bold.GetHashCode() ^ Italic.GetHashCode() ^ Underline.GetHashCode() ^ StrikeThrough.GetHashCode() ^
                    Shadow.GetHashCode() ^ FontScheme.GetHashCode() ^ Outline.GetHashCode() ^ Extend.GetHashCode() ^
                    Codense.GetHashCode() ^ Extend.GetHashCode() ^ Color.GetHashCode();
        }

        public static Font LoadFromXml (XmlNode node)
        {
            return Loader.Load<Font>(node);
        }
	}
}

