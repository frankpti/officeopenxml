/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("cellStyle")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class CellStyle
    {
        public CellStyle()
        {
            BuiltInStyleId = 0;
            CustomBuiltIn = false;
            HiddenStyle = false;
        }

        [YAXSerializableField]
        [YAXSerializeAs("builtInId")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public uint BuiltInStyleId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("customBuiltin")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool CustomBuiltIn {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("hidden")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool HiddenStyle {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("iLevel")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public uint OutlineStyle {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("name")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public string UserDefinedCellStyle {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("xfId")]
        [YAXAttributeForClass]
        public uint FormatId {
            get;
            set;
        }

        public static bool operator == (CellStyle a, CellStyle b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.BuiltInStyleId == b.BuiltInStyleId && a.CustomBuiltIn == b.CustomBuiltIn &&
                    a.HiddenStyle == b.HiddenStyle && a.OutlineStyle == b.OutlineStyle &&
                    a.UserDefinedCellStyle == b.UserDefinedCellStyle && a.FormatId == b.FormatId;
        }

        public static bool operator != (CellStyle a, CellStyle b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[CellStyle: BuiltInStyleId={0}, CustomBuiltIn={1}, HiddenStyle={2}, OutlineStyle={3}, UserDefinedCellStyle={4}, FormatId={5}]", BuiltInStyleId, CustomBuiltIn, HiddenStyle, OutlineStyle, UserDefinedCellStyle, FormatId);
        }

        public override bool Equals (object obj)
        {
            if (obj is CellStyle) {
                return ((obj as CellStyle) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return BuiltInStyleId.GetHashCode() ^ CustomBuiltIn.GetHashCode() ^ HiddenStyle.GetHashCode() ^
                OutlineStyle.GetHashCode() ^ UserDefinedCellStyle.GetHashCode() ^ FormatId.GetHashCode();
        }

        public static CellStyle LoadFromXml (XmlNode node)
        {
            return Loader.Load<CellStyle>(node);
        }
    }
}

