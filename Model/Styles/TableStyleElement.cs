/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("tableStyleElement")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
	public class TableStyleElement
	{
        public TableStyleElement()
        {
        }

        public TableStyleElement(TableStyleType styleType)
        {
            StyleType = styleType;
        }

        [YAXSerializableField]
        [YAXSerializeAs("dxfId")]
        [YAXAttributeFor("tableStyleElement")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public int FormattingId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("size")]
        [YAXAttributeFor("tableStyleElement")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public uint BandSize {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("type")]
        [YAXAttributeFor("tableStyleElement")]
        public TableStyleType StyleType {
            get;
            set;
        }

        public static bool operator == (TableStyleElement a, TableStyleElement b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.FormattingId == b.FormattingId && a.BandSize == b.BandSize && a.StyleType == b.StyleType;
        }

        public static bool operator != (TableStyleElement a, TableStyleElement b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[TableStyleElement: FormattingId={0}, BandSize={1}, StyleType={2}]", FormattingId, BandSize, StyleType);
        }

        public override bool Equals (object obj)
        {
            if (obj is TableStyleElement) {
                return ((obj as TableStyleElement) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return FormattingId.GetHashCode() ^ BandSize.GetHashCode() ^ StyleType.GetHashCode();
        }
	}
}

