/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using YAXLib;
using DocumentFormat.OpenXml.Parts;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("styleSheet")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class StyleSheet: PackageXmlSerializable, IPart
    {
        internal const string DefaultDocumentPath = "/xl/styles.xml";

        private const uint NumberFormatStart = 164;

        public StyleSheet ()
        {
        }

        public StyleSheet(string relId):
            this()
        {
            RelId = relId;
        }

        [YAXSerializableField]
        [YAXSerializeAs("numFmts")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "numFmt")]
        public List<NumberFormat> NumberFormats {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("numFmts")]
        public int NumberFormatsCount {
            get {
                return NumberFormats != null ? NumberFormats.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("fonts")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "font")]
        public List<Font> Fonts {
            get;
            set;
        }
                
        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("fonts")]
        public int FontsCount {
            get {
                return Fonts != null ? Fonts.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("fills")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "fill")]
        public List<Fill> Fills {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("fills")]
        public int FillsCount {
            get {
                return Fills != null ? Fills.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("borders")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "border")]
        public List<Border> Borders {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("borders")]
        public int BordersCount {
            get {
                return Borders != null ? Borders.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("cellStyleXfs")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "xf")]
        public List<FormattingRecordsXf> FormattingRecords {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("cellStyleXfs")]
        public int FormattingRecordsCount {
            get {
                return FormattingRecords != null ? FormattingRecords.Count : 0;
            }
        }
                
        [YAXSerializableField]
        [YAXSerializeAs("cellXfs")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "xf")]
        public List<FormatXf> CellFormats {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("cellXfs")]
        public int CellFormatsCount {
            get {
                return CellFormats != null ? CellFormats.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("colors")]
        public Colors Colors {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("cellStyles")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "cellStyle")]
        public List<CellStyle> CellStyles {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("cellStyles")]
        public int CellStylesCount {
            get {
                return CellStyles != null ? CellStyles.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("dxfs")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "dxf")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public List<Formatting> Formats {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("dxfs")]
        public int FormatsCount {
            get {
                return TableStyles != null ? Formats.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("tableStyles")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "tableStyle")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public List<TableStyle> TableStyles {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("count")]
        [YAXAttributeFor("tableStyles")]
        public int TableStylesCount {
            get {
                return TableStyles != null ? TableStyles.Count : 0;
            }
        }

        [YAXSerializableField]
        [YAXSerializeAs("defaultPivotStyle")]
        [YAXAttributeFor("tableStyles")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public string DefaultPivotStyle {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("defaultTableStyle")]
        [YAXAttributeFor("tableStyles")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public string DefaultTableStyle {
            get;
            set;
        }

        #region implemented abstract members of ProbeNet.Mini.Export.OfficeOpenXml.Model.PackageXmlSerializable
        public override string ContentType {
            get {
                return ContentTypes.StylesXml;
            }
        }
        #endregion

        public uint GetIndexOf(CellFormat cellFormat)
        {
            FormattingRecordsXf xf = new FormattingRecordsXf();
            if (cellFormat.NumberFormat != null) {
                xf.ApplyNumberFormat = true;
                xf.NumberFormatId = GetIndexOf (cellFormat.NumberFormat);
            }
            if (cellFormat.Font != null) {
                xf.ApplyFont = true;
                xf.FontId = GetIndexOf (cellFormat.Font);
            }
            if (cellFormat.Fill != null) {
                xf.ApplyFill = true;
                xf.FillId = GetIndexOf (cellFormat.Fill);
            }
            if (cellFormat.Border != null) {
                xf.ApplyBorder = true;
                xf.BorderId = GetIndexOf (cellFormat.Border);
            }
            return GetIndexOf (xf, cellFormat.Caption);
        }

        private uint GetIndexOf(NumberFormat numberFormat)
        {
            if (NumberFormats == null) {
                NumberFormats = new List<NumberFormat>();
            }
            if (NumberFormats.Contains(numberFormat)) {
                int index = NumberFormats.IndexOf(numberFormat);
                return NumberFormats[index].Id;
            }
            uint id = NumberFormatStart + (uint)NumberFormats.Count;
            numberFormat.Id = id;
            NumberFormats.Add(numberFormat);
            return id;
        }

        private uint GetIndexOf(Font font)
        {
            if (Fonts == null) {
                Fonts = new List<Font>();
            }
            if (Fonts.Contains(font)) {
                return (uint)Fonts.IndexOf(font);
            }
            Fonts.Add(font);
            return (uint)(Fonts.Count - 1);
        }

        private uint GetIndexOf(Fill fill)
        {
            if (Fills == null) {
                Fills = new List<Fill>();
                Fills.Add(new Fill(new PatternFill()));
                Fills.Add(new Fill(new PatternFill()));
            }
            if (Fills.Contains(fill)) {
                return (uint)Fills.IndexOf(fill);
            }
            Fills.Add(fill);
            return (uint)(Fills.Count - 1);
        }

        private uint GetIndexOf(Border border)
        {
            if (Borders == null) {
                Borders = new List<Border>();
            }
            if (Borders.Contains(border)) {
                return (uint)Borders.IndexOf(border);
            }
            Borders.Add(border);
            return (uint)(Borders.Count - 1);
        }

        private uint GetIndexOf(FormattingRecordsXf xf, string formatCaption)
        {
            uint formatRecordId = 0;
            if (FormattingRecords == null) {
                FormattingRecords = new List<FormattingRecordsXf>();
            }
            if (FormattingRecords.Contains(xf)) {
                formatRecordId = (uint)FormattingRecords.IndexOf(xf);
            } else {
                FormattingRecords.Add(xf);
                formatRecordId = (uint)(FormattingRecords.Count - 1);
            }
            if (!String.IsNullOrEmpty(formatCaption)) {
                CellStyle cellStyle = new CellStyle() {
                    FormatId = formatRecordId,
                    UserDefinedCellStyle = formatCaption
                };
                if (CellStyles == null) {
                    CellStyles = new List<CellStyle>();
                }
                if (!CellStyles.Contains(cellStyle)) {
                    CellStyles.Add(cellStyle);
                }
            }
            if (CellFormats == null) {
                CellFormats = new List<FormatXf>();
            }
            FormatXf formatXf = new FormatXf(xf, formatRecordId);
            if (CellFormats.Contains(formatXf)) {
                return (uint)CellFormats.IndexOf(formatXf);
            }
            CellFormats.Add(formatXf);
            return (uint)(CellFormats.Count - 1);
        }

        private void AddToFormattingRecords (FormattingRecordsXf xf)
        {
            if (!FormattingRecords.Contains(xf)) {
                FormattingRecords.Add(xf);
            }
        }

        public void Serialize (Stream stream)
        {
            SerializeStreamXml(stream);
        }

        public string DocumentPath {
            get {
                return DefaultDocumentPath;
            }
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipStyles;
            }
        }
    }
}

