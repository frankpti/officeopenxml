/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("protection")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Protection
    {
        public Protection ()
        {
        }

        [YAXSerializableField]
        [YAXSerializeAs("locked")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool Locked {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("hidden")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool Hidden {
            get;
            set;
        }


        public static bool operator == (Protection a, Protection b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.Hidden == b.Hidden && a.Locked == b.Locked;
        }

        public static bool operator != (Protection a, Protection b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Protection: Locked={0}, Hidden={1}]", Locked, Hidden);
        }

        public override bool Equals (object obj)
        {
            if (obj is Protection) {
                return ((obj as Protection) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return Locked.GetHashCode() ^ Hidden.GetHashCode();
        }

        public static Protection LoadFromXml (XmlNode node)
        {
            return Loader.Load<Protection>(node);
        }
    }
}

