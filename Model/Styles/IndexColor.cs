/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("rgbColor")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class IndexColor
    {
        private uint argbData;

        public IndexColor ():
            this(0)
        {
        }

        public IndexColor(uint argbData)
        {
            ArgbData = argbData;
        }

        public IndexColor(string argb)
        {
            Argb = argb;
        }

        [YAXSerializableField]
        [YAXSerializeAs("rgb")]
        [YAXAttributeForClass]
        public string Argb {
            get {
                return String.Format("{0:X}", ArgbData);
            }
            set {
                ArgbData = uint.Parse(value, System.Globalization.NumberStyles.AllowHexSpecifier);
            }
        }

        public uint ArgbData {
            get {
                return argbData;
            }
            set {
                argbData = value & 0xFFFFFFFF;
            }
        }

        public static bool operator == (IndexColor a, IndexColor b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.ArgbData == b.ArgbData;
        }

        public static bool operator != (IndexColor a, IndexColor b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[IndexColor: Argb={0}, ArgbData={1}]", Argb, ArgbData);
        }

        public override bool Equals (object obj)
        {
            if (obj is IndexColor) {
                return ((obj as IndexColor) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return ArgbData.GetHashCode();
        }
    }
}

