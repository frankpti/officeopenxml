/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("numFmt")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class NumberFormat
    {
        public NumberFormat():
            this(String.Empty, 0)
        {
        }

        public NumberFormat(string formatCode, uint id)
        {
            FormatCode = formatCode;
            Id = id;
        }

        [YAXSerializableField]
        [YAXSerializeAs("formatCode")]
        [YAXAttributeForClass]
        public string FormatCode {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("numFmtId")]
        [YAXAttributeForClass]
        public uint Id {
            get;
            set;
        }

        public static bool operator == (NumberFormat a, NumberFormat b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            if (a.Id == b.Id) {
                return true;
            }
            return a.FormatCode == b.FormatCode;
        }

        public static bool operator != (NumberFormat a, NumberFormat b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[NumberFormat: FormatCode={0}, Id={1}]", FormatCode, Id);
        }

        public override bool Equals (object obj)
        {
            if (obj is NumberFormat) {
                return ((obj as NumberFormat) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return FormatCode.GetHashCode() ^ Id.GetHashCode();
        }

        public static NumberFormat LoadFromXml (XmlNode node)
        {
            return Loader.Load<NumberFormat>(node);
        }
    }
}

