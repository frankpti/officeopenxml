/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;
using DocumentFormat.OpenXml.Model.Worksheets;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("borderItem")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class BorderItem
    {
        public BorderItem()
        {
            Style = LineStyle.None;
        }

        [YAXSerializableField]
        [YAXSerializeAs("color")]
        public Color Color {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXAttributeForClass]
        [YAXSerializeAs("style")]
        public LineStyle Style {
            get;
            set;
        }

        public static bool operator == (BorderItem a, BorderItem b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.Color == b.Color && a.Style == b.Style;
        }

        public static bool operator != (BorderItem a, BorderItem b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[BorderItem: Color={0}, Style={1}]", Color, Style);
        }

        public override bool Equals (object obj)
        {
            if (obj is BorderItem) {
                return ((obj as BorderItem) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return Color.GetHashCode() ^ Style.GetHashCode();
        }

        public static BorderItem LoadFromXml(XmlNode node)
        {
            return Loader.Load<BorderItem>(node);
        }
    }
}

