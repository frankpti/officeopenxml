/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("dxf")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Formatting
    {
        [YAXSerializableField]
        [YAXSerializeAs("alignment")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Alignment Alignment {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("border")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Border Border {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("fill")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Fill Fill {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("font")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Font Font {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("numFmt")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public NumberFormat NumberFormat {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("protection")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public Protection Protection {
            get;
            set;
        }

        public static bool operator == (Formatting a, Formatting b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.Alignment == b.Alignment && a.Border == b.Border && a.Fill == b.Fill && a.Font == b.Font &&
                a.NumberFormat == b.NumberFormat && a.Protection == b.Protection;
        }

        public static bool operator != (Formatting a, Formatting b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Formatting: Alignment={0}, Border={1}, Fill={2}, Font={3}, NumberFormat={4}, Protection={5}]", Alignment, Border, Fill, Font, NumberFormat, Protection);
        }

        public override bool Equals (object obj)
        {
            if (obj is Formatting) {
                return ((obj as Formatting) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return Alignment.GetHashCode() ^ Border.GetHashCode() ^ Fill.GetHashCode() ^ Font.GetHashCode() ^
                NumberFormat.GetHashCode() ^ Protection.GetHashCode();
        }

        public static Formatting LoadFromXml (XmlNode node)
        {
            return Loader.Load<Formatting>(node);
        }
    }
}

