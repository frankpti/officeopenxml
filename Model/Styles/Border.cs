/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using YAXLib;
using System.Xml;
using DocumentFormat.OpenXml.XmlHandling;

namespace DocumentFormat.OpenXml.Model.Styles
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("border")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Border
    {
        public Border()
        {
            DiagonalUp = false;
            DiagonalDown = false;
            Outline = false;
            Left = new BorderItem();
            Right = new BorderItem();
            Top = new BorderItem();
            Bottom = new BorderItem();
            Diagonal = new BorderItem();
        }

        [YAXSerializableField]
        [YAXSerializeAs("diagonalDown")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool DiagonalDown {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("diagonalUp")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool DiagonalUp {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("outline")]
        [YAXAttributeForClass]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public bool Outline {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("left")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Left {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("right")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Right {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("top")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Top {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("bottom")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Bottom {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("diagonal")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Diagonal {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("vertical")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Vertical {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("horizontal")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore)]
        public BorderItem Horizontal {
            get;
            set;
        }

        public static bool operator == (Border a, Border b)
        {
            if (((object)a == null) && ((object)b == null)) {
                return true;
            }
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }
            return a.DiagonalDown == b.DiagonalDown && a.DiagonalUp == b.DiagonalUp && a.Outline == b.Outline &&
                a.Left == b.Left && a.Right == b.Right && a.Top == b.Top && a.Bottom == b.Bottom &&
                a.Diagonal == b.Diagonal && a.Vertical == b.Vertical && a.Horizontal == b.Horizontal;
        }

        public static bool operator != (Border a, Border b)
        {
            return !(a == b);
        }

        public override string ToString ()
        {
            return string.Format ("[Border: DiagonalDown={0}, DiagonalUp={1}, Outline={2}, Start={3}, End={4}, Top={5}, Bottom={6}, Diagonal={7}, Vertical={8}, Horizontal={9}]", DiagonalDown, DiagonalUp, Outline, Left, Right, Top, Bottom, Diagonal, Vertical, Horizontal);
        }

        public override bool Equals (object obj)
        {
            if (obj is Border) {
                return ((obj as Border) == this);
            }
            return false;
        }

        public override int GetHashCode ()
        {
            return DiagonalDown.GetHashCode() ^ DiagonalUp.GetHashCode() ^ Outline.GetHashCode() ^ Left.GetHashCode() ^
                    Right.GetHashCode() ^ Top.GetHashCode() ^ Bottom.GetHashCode() ^ Diagonal.GetHashCode() ^
                    Vertical.GetHashCode() ^ Horizontal.GetHashCode();
        }

        public static Border LoadFromXml (XmlNode node)
        {
            return Loader.Load<Border>(node);
        }
    }
}

