/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace DocumentFormat.OpenXml.Model
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("Relationship")]
    [YAXNamespace(XmlNamespaces.OfficeOpenPackageRelationships)]
    public class Relationship
    {
        [Obsolete("just used for yaxlib deserialization")]
        public Relationship()
        {
        }

        private Relationship (string id, string type, string target)
        {
            Id = id;
            Type = type;
            Target = target;
        }

        public static Relationship Create(Relationships parent, string type, string target)
        {
            Relationship r = new Relationship(parent.NextId(), type, target);
            parent[r.Id] = r;
            return r;
        }

        public static Relationship Create(Relationships parent, IPart part)
        {
            if (part.RelId == null) {
                part.RelId = parent.NextId();
            }
            Relationship r = new Relationship(part.RelId, part.Namespace, part.DocumentPath) {
                Part = part
            };
            parent[part.RelId] = r;
            return r;
        }

        [YAXSerializableField]
        [YAXSerializeAs("Id")]
        [YAXAttributeForClass]
        public string Id {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("Type")]
        [YAXAttributeForClass]
        public string Type {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("Target")]
        [YAXAttributeForClass]
        public string Target {
            get;
            set;
        }

        public IPart Part {
            get;
            set;
        }
    }
}

