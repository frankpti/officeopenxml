/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using DocumentFormat.OpenXml.Enums;

namespace DocumentFormat.OpenXml.Model.ChartDrawing
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("valAx")]
    [YAXNamespace("c", XmlNamespaces.OfficeOpenDrawingChart)]
	public class ValueAxis
	{
        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("axId")]
        public uint AxId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("scaling")]
        public Scaling Scaling {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("title")]
        public Title Title {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("axPos")]
        public AxisPositionType AxisPosition {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("delete")]
        public bool Delete {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("majorGridlines")]
        public GridLines MajorGridLines {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("majorTickMark")]
        public TickMarkType MajorTickMark {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("minorGridlines")]
        public GridLines MinorGridLines {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("minorTickMark")]
        public TickMarkType MinorTickMark {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("tickLblPos")]
        public TickLabelPositionType TickLabelPosition {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("crossAx")]
        public uint CrossingAxisId {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("val")]
        [YAXAttributeFor("crosses")]
        public CrossesType Crosses {
            get;
            set;
        }

        [YAXSerializableField]
        [YAXSerializeAs("spPr")]
        public ShapeProperties ShapeProperties {
            get;
            set;
        }
	}
}

