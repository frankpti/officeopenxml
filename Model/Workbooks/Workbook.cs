/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Model;

namespace DocumentFormat.OpenXml.Model.Workbooks
{
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("workbook")]
    [YAXNamespace(XmlNamespaces.OfficeOpenSpreadsheet)]
    public class Workbook: PackageXmlSerializable, IPart
    {
        internal const string DefaultDocumentPath = "/xl/workbook.xml";

        public Workbook ()
        {
            Sheets = new List<Sheet>();
        }

        [YAXSerializableField]
        [YAXSerializeAs("sheets")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "sheet")]
        public List<Sheet> Sheets {
            get;
            private set;
        }

        public override string ContentType
        {
            get {
                return ContentTypes.WorkbookXml;
            }
        }

        public int NextId ()
        {
            return Sheets.Count + 1;
        }

        public Sheet AppendSheet (string sheetName, string relId)
        {
            return Sheet.Create(this, sheetName, relId);
        }

        public Sheet FindSheetByRelId (string relId)
        {
            foreach (Sheet sheet in Sheets) {
                if (sheet.RelId == relId) {
                    return sheet;
                }
            }
            return null;
        }

        public Sheet FindSheetById (int sheetId)
        {
            foreach (Sheet sheet in Sheets) {
                if (sheet.SheetId == sheetId) {
                    return sheet;
                }
            }
            return null;
        }

        #region IPart implementation
        public void Serialize (System.IO.Stream stream)
        {
            SerializeStreamXml(stream);
        }

        public string DocumentPath {
            get {
                return DefaultDocumentPath;
            }
        }

        public string RelId {
            get;
            set;
        }

        public string Namespace {
            get {
                return XmlNamespaces.OfficeOpenRelationshipDocument;
            }
        }
        #endregion

    }
}

