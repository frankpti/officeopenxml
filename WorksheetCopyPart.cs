/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace DocumentFormat.OpenXml
{
	class WorksheetCopyPart : IPart
	{
        private Stream source;

		public WorksheetCopyPart (string path, string relId, Stream source)
		{
            DocumentPath = path;
            RelId = relId;
            this.source = source;
		}

		public string ContentType {
			get {
                return ContentTypes.WorksheetXml;
            }
		}

		public string DocumentPath {
            get;
            private set;
		}

		public string RelId {
			get;
			set;
		}

		public string Namespace {
			get {
                return XmlNamespaces.OfficeOpenRelationshipWorksheet;
            }
		}

		public void Serialize (Stream stream)
		{
            XmlDocument doc = new XmlDocument ();
            doc.Load (source);
            List<XmlNode> nodesList = new List<XmlNode>(doc.GetElementsByTagName("sheetViews").Cast<XmlNode>());
            foreach (XmlNode element in nodesList) {
                element.ParentNode.RemoveChild (element);
            }
            doc.Save(stream);
		}
	}
}

