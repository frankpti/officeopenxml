/*
 * A C# library for creating simple office open xml documents
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.IO.Packaging;
using DocumentFormat.OpenXml.Model;
using DocumentFormat.OpenXml.Model.WordProcessing;
using DocumentFormat.OpenXml.Model.WordProcessing.Styles;
using DocumentFormat.OpenXml.Model.WordProcessing.Settings;
using DocumentFormat.OpenXml.Model.WordProcessing.FontTable;

namespace DocumentFormat.OpenXml
{
	public class WordprocessingExport: ExportBase
	{
        private Document document;
        private StylesDocument styles;
        private DocumentSettings settings;
        private Fonts fonts;

		public WordprocessingExport (
            Document document, StylesDocument styles = null, DocumentSettings settings = null, Fonts fonts = null)
		{
            if (document == null) {
                document = new Document();
            }
            this.document = document;
            if (styles == null) {
                styles = new StylesDocument();
            }
            this.styles = styles;
            if (settings == null) {
                settings = new DocumentSettings();
            }
            this.settings = settings;
            if (fonts == null) {
                fonts = new Fonts();
            }
            this.fonts = fonts;
		}

        #region implemented abstract members of DocumentFormat.OpenXml.ExportBase
        protected override void FillZipPackage (Package package)
        {
            Relationships documentRelationships = new Relationships();

            //TODO: export metadata using new API

            //string propsPath = "/docProps/{0}.xml";
            //string corePropsPath = String.Format(propsPath, "core");
            //string appPropsPath = String.Format(propsPath, "app");
            // Metadata.ExportToPackage(package, corePropsPath, appPropsPath);

            Relationships mainRelationships = new Relationships();
            mainRelationships.Append(XmlNamespaces.OfficeOpenRelationshipDocument, "word/document.xml");
            mainRelationships.Append(XmlNamespaces.OfficeOpenRelationshipMetadataCoreProperties, "docProps/core.xml");
            mainRelationships.Append(XmlNamespaces.OfficeOpenRelationshipExtendedProperties, "docProps/app.xml");
            
            mainRelationships.ExportToPackage(package, "_rels/.rels");
            
            document.ExportToPackage(package, "word/document.xml");

            AddSettingsDocument(package, documentRelationships, settings);
            AddFontTableDocument(package, documentRelationships, fonts);
            AddStylesDocument(package, documentRelationships, styles);
            
            documentRelationships.ExportToPackage(package, "word/_rels/document.xml.rels");
        }
        #endregion

        private static void AddDocument<T>(
            Package package, Relationships documentRelationships, T document, string documentName, string nameSpace)
            where T: PackageXmlSerializable
        {
            document.ExportToPackage(package, String.Format("word/{0}", documentName));
            documentRelationships.Append(nameSpace, documentName);
        }

        private static void AddStylesDocument(
            Package package, Relationships documentRelationships, StylesDocument styles)
        {
            AddDocument<StylesDocument>(
                package, documentRelationships, styles, "styles.xml", XmlNamespaces.OfficeOpenRelationshipStyles);
        }

        private static void AddSettingsDocument(
            Package package, Relationships documentRelationships, DocumentSettings settings)
        {
            AddDocument<DocumentSettings>(
                package, documentRelationships, settings, "settings.xml", XmlNamespaces.OfficeOpenRelationshipSettings);
        }

        private static void AddFontTableDocument(Package package, Relationships documentRelationships, Fonts fonts)
        {
            AddDocument<Fonts>(
                package, documentRelationships, fonts, "fontTable.xml", XmlNamespaces.OfficeOpenRelationshipFontTable);
        }
	}
}

